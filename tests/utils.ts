import * as entities from '../src/entities';
import { Connection } from 'typeorm';
import { setupDBConnection } from '../src/db';
import { createApp as coreCreateApp } from '../src/http';
import { TaskRepository } from '../src/repositories';
import { Task } from '../src/entities';
import { Application } from 'express';
import { Logger, TaskStatus } from '../src/typedefs';
export const dbConnectio: Promise<Connection> = setupDBConnection();

const silentLogger: Logger = {
  info: () => {
    /**/
  },
  debug: () => {
    /**/
  },
  warning: () => {
    /**/
  },
  error: () => {
    /**/
  },
};

export function createApp(connection: Connection): Application {
  return coreCreateApp({
    taskRepository: connection.getCustomRepository(TaskRepository),
    logger: silentLogger,
  });
}

export async function clear(connection: Connection): Promise<void> {
  for (const entity of Object.values(entities)) {
    await connection.getRepository(entity).clear();
  }
}

export async function seed(connection: Connection): Promise<Task[]> {
  const repository = connection.getRepository(Task);

  const task1 = repository.create();
  task1.description = 'something1';
  task1.status = TaskStatus.active;
  task1.expiresAt = new Date(Date.UTC(2020, 0, 1));

  await repository.save(task1);

  const task2 = repository.create();
  task2.description = 'something2';
  task2.status = TaskStatus.completed;
  task2.expiresAt = new Date(Date.UTC(2020, 0, 2));

  await repository.save(task2);

  const task3 = repository.create();
  task3.description = 'something3';
  task3.status = TaskStatus.active;
  task3.expiresAt = new Date(Date.UTC(2020, 0, 3));

  await repository.save(task3);

  return [task1, task2, task3];
}

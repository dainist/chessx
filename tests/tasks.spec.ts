import { Connection } from 'typeorm';
import * as supertest from 'supertest';
import { dbConnectio, clear, createApp, seed } from './utils';
import { Server } from 'http';
import { TaskDto, TaskFilterDto } from '../src/dto';
import { TaskStatus } from '../src/typedefs';
import { Task } from '../src/entities';

describe('tasks', () => {
  let connection: Connection;
  let server: Server;

  beforeAll(async () => {
    connection = await dbConnectio;
  });

  afterAll(async () => {
    await connection.close();
  });

  beforeEach(async () => {
    await clear(connection);
    const app = createApp(connection);

    server = await new Promise<Server>((resolve) => {
      const httpServer = app.listen(() => {
        resolve(httpServer);
      });
    });
  });

  afterEach((done) => {
    server.close(done);
  });

  describe('POST /tasks', () => {
    it('should create new task', async () => {
      await supertest(server)
        .post('/tasks')
        .send({
          description: 'something',
          expireDate: '2020-01-01T00:00:00.000Z',
          status: TaskStatus.active,
        } as TaskDto)
        .then((response) => {
          expect(response.status).toBe(200);

          expect(response.body).toEqual({
            id: expect.any(Number),
            description: 'something',
            expiresAt: '2020-01-01T00:00:00.000Z',
            status: 'active',
            createdAt: expect.any(String),
          });
        });

      expect(await connection.getRepository(Task).count()).toBe(1);
      expect(await connection.getRepository(Task).findOne()).toEqual({
        id: expect.any(Number),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        description: 'something',
        status: 'active',
        expiresAt: new Date('2020-01-01T00:00:00.000Z'),
      });
    });

    it('should reject tasks with bad request error if provided task fails validation', async () => {
      await supertest(server)
        .post('/tasks')
        .send({
          description: '',
          expireDate: '2020-01-01T00:00:00.000Z',
          status: TaskStatus.active,
        })
        .expect(400);
    });
  });

  describe('PUT /tasks/:id', () => {
    let tasks: Task[];

    beforeEach(async () => {
      tasks = await seed(connection);
    });

    it('should be possible to edit already existing task', async () => {
      await supertest(server)
        .put(`/tasks/${tasks[2].id}`)
        .send({
          description: 'something666',
          expireDate: '2020-01-05T00:00:00.000Z',
          status: TaskStatus.completed,
        })
        .then((response) => {
          expect(response.status).toBe(200);

          expect(response.body).toEqual({
            id: tasks[2].id,
            status: TaskStatus.completed,
            description: 'something666',
            expiresAt: '2020-01-05T00:00:00.000Z',
            createdAt: expect.any(String),
          });
        });

      expect(
        (await connection.getRepository(Task).findOne(tasks[0].id)).status,
      ).toBe(TaskStatus.active);

      expect(await connection.getRepository(Task).findOne(tasks[2].id)).toEqual(
        {
          id: tasks[2].id,
          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),
          expiresAt: expect.any(Date),
          description: 'something666',
          status: TaskStatus.completed,
        },
      );
    });

    it('should reject tasks with bad request error if provided task fails validation', async () => {
      await supertest(server)
        .put(`/tasks/${tasks[2].id}`)
        .send({
          description: '',
          expireDate: '2020-01-05T00:00:00.000Z',
          status: TaskStatus.completed,
        })
        .expect(400);
    });

    it("should return not found error if task does't exist", async () => {
      await supertest(server)
        .put('/tasks/666')
        .send({
          description: 'something',
          expireDate: '2020-01-01T00:00:00.000Z',
          status: TaskStatus.active,
        } as TaskDto)
        .expect(404);
    });

    it("should return not found error if provided task id isn't a number", async () => {
      await supertest(server)
        .put('/tasks/something')
        .send({
          description: 'something666',
          expireDate: '2020-01-05T00:00:00.000Z',
          status: TaskStatus.completed,
        })
        .expect(404);
    });
  });

  describe('GET /tasks/:id', () => {
    let tasks: Task[];

    beforeEach(async () => {
      tasks = await seed(connection);
    });

    it('should be possible to retrieve a task', async () => {
      await supertest(server)
        .get(`/tasks/${tasks[0].id}`)
        .then((response) => {
          expect(response.status).toBe(200);

          expect(response.body).toEqual({
            id: tasks[0].id,
            description: 'something1',
            status: TaskStatus.active,
            expiresAt: '2020-01-01T00:00:00.000Z',
            createdAt: expect.any(String),
          });
        });
    });

    it("should return bad request error if task does't exist", async () => {
      await supertest(server).get('/tasks/666').expect(404);
    });

    it("should return bad request error if id isn't a number", async () => {
      await supertest(server).get('/tasks/something').expect(404);
    });
  });

  describe('DEL /tasks/:id', () => {
    let tasks: Task[];

    beforeEach(async () => {
      tasks = await seed(connection);
    });

    it('should be possible to delete a task', async () => {
      await supertest(server).del(`/tasks/${tasks[1].id}`).expect(200);

      expect(await connection.getRepository(Task).count()).toBe(2);
      expect(await connection.getRepository(Task).findOne(tasks[1].id)).toBe(
        undefined,
      );
    });

    it("should return not found error if task does't exist", async () => {
      await supertest(server).del('/tasks/666').expect(404);
    });

    it("should return not found error if task id isn't a number", async () => {
      await supertest(server).del('/tasks/something').expect(404);
    });
  });

  describe('POST /tasks/filter', () => {
    let tasks: Task[];

    beforeEach(async () => {
      tasks = await seed(connection);
    });

    it('should be possible to filter tasks with required status', async () => {
      await supertest(server)
        .post('/tasks/filter')
        .send({
          status: TaskStatus.active,
        } as TaskFilterDto)
        .then((response) => {
          expect(response.status).toBe(200);

          expect(response.body.length).toBe(2);

          expect(response.body).toContainEqual({
            id: tasks[0].id,
            createdAt: expect.any(String),
            description: 'something1',
            expiresAt: '2020-01-01T00:00:00.000Z',
            status: TaskStatus.active,
          });
          expect(response.body).toContainEqual({
            id: tasks[2].id,
            createdAt: expect.any(String),
            description: 'something3',
            expiresAt: '2020-01-03T00:00:00.000Z',
            status: TaskStatus.active,
          });
        });
    });
  });
});

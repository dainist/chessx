# About

Solution is using mysql for the storage.

Entry point for the application is ```src/index.ts```

Rest api endpoints can be be seen in ```/src/routes```

# Running

* ```npm run compile && npm run start``` for compiled version
* ```npm run test:e2e``` to run e2e tests for the API

# Configuration

Configuration is done through environment variables.

Sample ```.env.dist``` and ```.env.test.dist```
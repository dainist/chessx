import { Logger, LoggingFunction } from '../typedefs';

const logStdOut = (level: string): LoggingFunction => {
  return (tag: string, msg: string, ...args: unknown[]): void => {
    console.log(`${level} ${tag}:${msg} ${JSON.stringify(args, null, 2)}`);
  };
};

const logStdErr = (level: string): LoggingFunction => {
  return (tag: string, msg: string, ...args: unknown[]): void => {
    console.warn(`${level} ${tag}:${msg} ${JSON.stringify(args, null, 2)}`);
  };
};

export const consoleLogger: Logger = {
  debug: logStdOut('debug'),
  info: logStdOut('info'),
  warning: logStdErr('warning'),
  error: logStdErr('error'),
};

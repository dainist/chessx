import { config } from 'dotenv';
import { setupDBConnection } from './db';
import { createApp } from './http';
import { TaskRepository } from './repositories';
import { consoleLogger } from './utils';

config();

(async (): Promise<void> => {
  const connection = await setupDBConnection();

  const app = createApp({
    taskRepository: connection.getCustomRepository(TaskRepository),
    logger: consoleLogger,
  });

  app.listen(process.env.PORT, (error) => {
    if (error) {
      throw error;
    }

    consoleLogger.info('http', 'Listening');
  });
})();

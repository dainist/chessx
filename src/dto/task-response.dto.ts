import { Task } from '../entities';

export type TaskResponseDto = Pick<
  Task,
  'description' | 'id' | 'status' | 'createdAt' | 'expiresAt'
>;

import { IsIn } from 'class-validator';
import { TaskStatus } from '../typedefs';

export class TaskFilterDto {
  @IsIn(Object.values(TaskStatus))
  status: TaskStatus;
}

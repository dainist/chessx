import { IsDateString, IsString, Length, IsIn } from 'class-validator';
import { TaskStatus, DESCRIPTION_LENGTH } from '../typedefs';

export class TaskDto {
  @Length(1, DESCRIPTION_LENGTH)
  @IsString()
  readonly description: string;

  @IsDateString()
  readonly expireDate: string;

  @IsIn(Object.values(TaskStatus))
  readonly status: TaskStatus;
}

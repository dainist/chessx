import { ErrorRequestHandler } from 'express';
import { Logger } from '../typedefs';

export function errorLogger(logger: Logger): ErrorRequestHandler {
  return (error, req, resp, next): void => {
    logger.error('http', 'Request failed with an error', error.toString(), {
      url: req.url,
      body: req.body,
    });

    next(error);
  };
}

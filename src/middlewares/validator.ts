import { RequestHandler, Request } from 'express';
import { Type } from '../typedefs';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

export function requestValidator<Dto>(dto: Type<Dto>): RequestHandler {
  return async (
    req: Request<Record<string, string>, unknown, Dto>,
    resp,
    next,
  ): Promise<void> => {
    const obj = plainToClass(dto, req.body);

    const error = await validate(obj);

    // class validator returns an array - each failed fields gets its own
    // error
    if (error.length > 0) {
      return next(error);
    }

    req.body = obj;

    return next();
  };
}

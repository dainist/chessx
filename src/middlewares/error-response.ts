import { ErrorRequestHandler } from 'express';
import { ValidationError } from 'class-validator';
import { ErrorResponseDto } from '../dto';
import { NotFoundError } from '../utils';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorResponse: ErrorRequestHandler = (error, req, resp, next) => {
  if (Array.isArray(error) && error[0] instanceof ValidationError) {
    return resp.status(400).json({
      status: 400,
      message: error.toString(),
    } as ErrorResponseDto);
  }

  if (error instanceof NotFoundError) {
    return resp.status(404).json({
      status: 404,
      message: error.toString(),
    } as ErrorResponseDto);
  }

  return resp.status(500).json({
    status: 500,
    message: 'Internal server error, try again',
  } as ErrorResponseDto);
};

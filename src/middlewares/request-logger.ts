import { Handler } from 'express';
import { Logger } from '../typedefs';

export function requestLogger(logger: Logger): Handler {
  return (req, resp, next): void => {
    logger.info('http', 'Received new request', {
      method: req.method,
      url: req.url,
      body: req.body,
      query: req.query,
    });

    next();
  };
}

import * as express from 'express';

export type Action<BODY, RESPONSE> = (
  req: express.Request<Record<string, string>, unknown, BODY>,
) => Promise<RESPONSE>;

/**
 * To work around the nature of express, it doesn't play nice
 * with async/await and errors being thrown
 *
 * @type {express.Handler}
 */
export function asyncWrapper<BODY, RESPONSE>(
  action: Action<BODY, RESPONSE>,
): express.Handler {
  return async (req, resp, next): Promise<void> => {
    try {
      const result = await action(req);

      resp.json(result);
    } catch (error) {
      next(error);
    }
  };
}

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DESCRIPTION_LENGTH } from '../typedefs';
import { TaskStatus } from '../typedefs';

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: DESCRIPTION_LENGTH })
  description: string;

  @Column({
    type: 'enum',
    enum: Object.values(TaskStatus),
    default: TaskStatus.active,
  })
  status: TaskStatus;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  expiresAt: Date;
}

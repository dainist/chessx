export enum TaskStatus {
  active = 'active',
  completed = 'completed',
}

export interface Type<T> extends Function {
  new (...args: unknown[]): T;
}

export type LoggingFunction = (
  tag: string,
  message: string,
  ...args: unknown[]
) => void;

export interface Logger {
  debug: LoggingFunction;
  info: LoggingFunction;
  warning: LoggingFunction;
  error: LoggingFunction;
}

export const DESCRIPTION_LENGTH = 500;

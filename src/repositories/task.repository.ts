import { EntityRepository, Repository } from 'typeorm';
import { Task } from '../entities';
import { TaskRepositoryInterface } from './typedefs';
import { TaskDto, TaskFilterDto } from '../dto';

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>
  implements TaskRepositoryInterface {
  async get(id: number): Promise<Task> {
    return this.findOne(id);
  }

  async edit(id: number, dto: TaskDto): Promise<Task> {
    const task = await this.findOne(id);

    if (!task) {
      return null;
    }

    task.description = dto.description;
    task.expiresAt = new Date(dto.expireDate);
    task.status = dto.status;

    return this.save(task);
  }

  async addNew(dto: TaskDto): Promise<Task> {
    const task = this.create();
    task.description = dto.description;
    task.status = dto.status;
    task.expiresAt = new Date(dto.expireDate);

    return this.save(task);
  }

  async removeById(id: number): Promise<boolean> {
    const result = await this.delete({
      id,
    });

    return result.affected > 0;
  }

  async filter(filter: TaskFilterDto): Promise<Task[]> {
    return this.find({
      status: filter.status,
    });
  }
}

import { Task } from '../entities';
import { TaskDto, TaskFilterDto } from '../dto';

export interface TaskRepositoryInterface {
  addNew(task: TaskDto): Promise<Task>;

  edit(id: number, task: TaskDto): Promise<Task>;

  get(id: number): Promise<Task>;
  /**
   * Removes task from DB
   *
   * Returns true if task previously existed
   *
   * @param  {number}           id
   * @return {Promise<boolean>}
   */
  removeById(id: number): Promise<boolean>;

  filter(filter: TaskFilterDto): Promise<Task[]>;
}

import { Router, Request, Response, NextFunction } from 'express';
import { asyncWrapper, requestValidator } from '../middlewares';
import { TaskDto, TaskResponseDto, TaskFilterDto } from '../dto';
import { TaskRepositoryInterface } from '../repositories';
import { Task } from '../entities';
import { NotFoundError } from '../utils';

const transformTask = (entity: Task): TaskResponseDto => ({
  id: entity.id,
  description: entity.description,
  createdAt: entity.createdAt,
  expiresAt: entity.expiresAt,
  status: entity.status,
});

function expectNumberId(
  req: Request,
  resp: Response,
  next: NextFunction,
): void {
  const id = parseInt(req.params.id, 10);

  if (String(id) != req.params.id) {
    return next(new NotFoundError('Task not found'));
  }

  return next();
}

export function createTaskRouter(
  taskRepository: TaskRepositoryInterface,
): Router {
  const router = Router();

  router.post(
    '/',
    requestValidator(TaskDto),
    asyncWrapper<TaskDto, TaskResponseDto>(async (req) => {
      const task = await taskRepository.addNew(req.body);

      return transformTask(task);
    }),
  );

  router.put(
    '/:id',
    expectNumberId,
    requestValidator(TaskDto),
    asyncWrapper<TaskDto, TaskResponseDto>(async (req) => {
      const id = parseInt(req.params.id, 10);

      const task = await taskRepository.edit(id, req.body);

      if (!task) {
        throw new NotFoundError('Task not found');
      }

      return transformTask(task);
    }),
  );

  router.get(
    '/:id',
    expectNumberId,
    asyncWrapper<null, TaskResponseDto>(async (req) => {
      const id = parseInt(req.params.id, 10);

      const task = await taskRepository.get(id);

      if (!task) {
        throw new NotFoundError('Task not found');
      }

      return transformTask(task);
    }),
  );

  router.delete(
    '/:id',
    expectNumberId,
    asyncWrapper<null, {}>(async (req) => {
      const id = parseInt(req.params.id, 10);

      const removed = await taskRepository.removeById(id);

      if (!removed) {
        throw new NotFoundError('Task not found');
      }

      return {};
    }),
  );

  router.post(
    '/filter',
    requestValidator(TaskFilterDto),
    asyncWrapper<TaskFilterDto, TaskResponseDto[]>(async (req) => {
      const tasks = await taskRepository.filter(req.body);

      return tasks.map(transformTask);
    }),
  );

  return router;
}

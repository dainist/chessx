import { createConnection, Connection } from 'typeorm';
import { resolve } from 'path';

export async function setupDBConnection(): Promise<Connection> {
  return createConnection({
    type: 'mysql',
    entities: [resolve(__dirname, 'entities/*.entity.{js,ts}')],
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    synchronize: process.env.DB_SYNC === 'true',
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PWD,
  });
}

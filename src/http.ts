import * as express from 'express';
import { json as jsonBodyParser } from 'body-parser';
import { errorLogger, errorResponse, requestLogger } from './middlewares';
import { TaskRepositoryInterface } from './repositories';
import { createTaskRouter } from './routers';
import { Logger } from './typedefs';

export function createApp(params: {
  taskRepository: TaskRepositoryInterface;
  logger: Logger;
}): express.Application {
  const tasks = createTaskRouter(params.taskRepository);

  const app = express();

  app.use(requestLogger(params.logger));
  app.use(jsonBodyParser());

  app.use('/tasks', tasks);

  app.use(errorLogger(params.logger));
  app.use(errorResponse);

  return app;
}
